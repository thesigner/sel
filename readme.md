# DEMO
Demo is available [here](http://thesigner.pl/sel/)


# USAGE
Simply include the script

```
<script src="src/sel.js"></script>
```

And use it by adding data attributes to your HTML elements

```
data-sel-in="driveInLeft" 
```


```
 data-sel-speed="600"
```


```
 data-sel-delay="600"
```

# ANIMATIONS IN

```
popIn
popInLeft
popInRight
popInTop
popInBottom
flipInX
flipInY
jumpInLeft
jumpInRight
swoopInLeft
swoopInRight
swoopInTop
swoopInBottom
driveInLeft
driveInRight
driveInTop
driveInBottom
fadeIn
fadeInLeft
fadeInRight
fadeInTop
fadeInBottom
rollInLeft
rollInRight
rollInTop
rollInBottom
spinIn
fold
pullLeft
pullRight
pullUp
pullDown
```


# ANIMATIONS OUT

```
popOut
popOutLeft
popOutRight
popOutTop
popOutBottom
flipOutX
flipOutY
jumpOutLeft
jumpOutRight
swoopOutLeft
swoopOutRight
swoopOutTop
swoopOutBottom
driveOutRight
driveOutLeft
driveOutTop
driveOutBottom
fadeOut
fadeOutLeft
fadeOutRight
fadeOutTop
fadeOutBottom
rollOutLeft
rollOutRight
rollOutTop
rollOutBottom
spinOut
unfold
fadeOut
fadeOut
fadeOut
fadeOut
```