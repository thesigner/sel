/**
 *
 * @project        selJS
 * @file           sel.js
 * @description    Animates elements usig CSS transitions.
 * @author         Michał Gwóźdź - thesigner
 * @version        1.0
 * 
 */

var ms = "ms";
var el;
var els;
var animation;
var speed;
var delay;
// options - more to do
var elspeed;
var eldelay;

/**
 * Gets all elements by data-attr
 */
els = document.querySelectorAll('[data-sel]');


/**
 * @description Sets basic css transition properties
 */
function initParams() {
	for (var i = 0; i < els.length; i++) {
		el = els[i];
		elspeed = el.getAttribute('data-sel-speed');
		eldelay = el.getAttribute('data-sel-delay');

		// sets element duration and delay
		speed = elspeed + ms;
		delay = eldelay + ms;

		if (speed && delay) {
			el.style.animationDelay = delay;
			el.style.animationDuration = speed;
		}
	}
}



/**
 * @description Checks if elment is visible in viewport
 * this part needs a redo w/o jq
 * @param element
 * @return boolean
 */
function isOnScreen(el) {
	var elementTop = $(el).offset().top;
	var elementBottom = elementTop + el.offsetHeight;
	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + window.innerHeight;
	return elementBottom > viewportTop && elementTop < viewportBottom;
}

/**
 * @description Handles each element as we scroll
 * Toggles animations (class)
 * this needs some throttling
 */
function handleScroll() {
	for (var j = 0; j < els.length; j++) {
		el = els[j];
		animation = el.getAttribute('data-sel');
		
		if (isOnScreen(el)) {
			el.classList.add(animation);
		} else {
			el.classList.remove(animation);
		}
	}
}

(function () {
	document.addEventListener('scroll', handleScroll);
	document.addEventListener('DOMContentLoaded', handleScroll);
	document.addEventListener('DOMContentLoaded', initParams);
})();