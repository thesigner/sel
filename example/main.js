jQuery(document).ready(function ($) {
	var animationsIn = [
		"popIn",
		"popInLeft",
		"popInRight",
		"popInTop",
		"popInBottom",
		"flipInX",
		"flipInY",
		"jumpInLeft",
		"jumpInRight",
		"swoopInLeft",
		"swoopInRight",
		"swoopInTop",
		"swoopInBottom",
		"driveInLeft",
		"driveInRight",
		"driveInTop",
		"driveInBottom",
		"fadeIn",
		"fadeInLeft",
		"fadeInRight",
		"fadeInTop",
		"fadeInBottom",
		"rollInLeft",
		"rollInRight",
		"rollInTop",
		"rollInBottom",
		"spinIn",
		"unfold",
		"pullLeft",
		"pullRight",
		"pullUp",
		"pullDown"
	];

	var animationsOut = [
		"popOut",
		"popOutLeft",
		"popOutRight",
		"popOutTop",
		"popOutBottom",
		"flipOutX",
		"flipOutY",
		"jumpOutLeft",
		"jumpOutRight",
		"swoopOutLeft",
		"swoopOutRight",
		"swoopOutTop",
		"swoopOutBottom",
		"driveOutRight",
		"driveOutLeft",
		"driveOutTop",
		"driveOutBottom",
		"fadeOut",
		"fadeOutLeft",
		"fadeOutRight",
		"fadeOutTop",
		"fadeOutBottom",
		"rollOutLeft",
		"rollOutRight",
		"rollOutTop",
		"rollOutBottom",
		"spinOut",
		"fold",
		"fadeOut",
		"fadeOut",
		"fadeOut",
		"fadeOut",
	];

	for (var i = 0; i < animationsIn.length; i++) {
		var animationIn = animationsIn[i];
		var animationOut = animationsOut[i];
		$(".select1").append("<option value='" + animationIn + "'>" + animationIn + "</div>");
		$(".select2").append("<option value='" + animationOut + "'>" + animationOut + "</div>");

	}

	$(".select3").keyup(function (e) {
		e.preventDefault();
		var duration = $(this).val();
		$(".sel").attr('style', 'animation-duration: ' + duration + 'ms;');
	});

	$(".select1, .select2").change(function (e) {
		e.preventDefault();
		var animation = $(this).val();
		$(".sel").attr('data-sel', animation);

		for (var i = 0; i < animationsIn.length; i++) {
			var animationIn = animationsIn[i];
			$(".sel").removeClass(animationIn);
		}

		for (var i = 0; i < animationsOut.length; i++) {
			var animationOut = animationsOut[i];
			$(".sel").removeClass(animationOut); 
		}
		$(".sel").addClass(animation);

	});


	var animate = true;
	var colors = ['#0477a5', '#637ce6'];

	$(window).scroll(function () {
		var color = colors[Math.floor(Math.random() * colors.length)];
		if (animate == true) {
			$("body").css("background", "" + color + "");
			setTimeout(function () {
				animate = true;
			}, 1000);
		}
		animate = false;
	});

	var vh = $(window).height();



});